import { Component} from '@angular/core';

@Component({
  selector: 'app-contador',
  template:`
  <h1>
  {{title}}
  </h1>
  <h3>La base es <strong>{{base}}</strong></h3>

  <button (click)="acomular(base)">+{{base}}</button>
  <strong>{{numero}}</strong>
  <button (click)="acomular(-base)">-{{base}}</button>
  `
})
export class ContadorComponent{
  public numero: number =0;
  title = 'Curso';
  public base: number = 5;

  acomular(numero: number){
    this.numero+=numero;
  }

}
